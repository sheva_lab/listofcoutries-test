package org.shevchenko.listofcountries.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.shevchenko.listofcountries.R;
import org.shevchenko.listofcountries.model.Country;

import java.util.ArrayList;


public class CountriesAdapter extends RecyclerView.Adapter<CountriesAdapter.UserViewHolder> {

    private Context mContext;
    private ArrayList<Country> mDataSet;

    public CountriesAdapter(Context mContext, ArrayList<Country> mDataSet) {
        this.mContext = mContext;
        this.mDataSet = mDataSet;
        setmDataSet(mDataSet);
    }

    public void setmDataSet(ArrayList<Country> mDataSet) {
        this.mDataSet = mDataSet;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.country_row, parent, false);
        return new UserViewHolder(v);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        final String nameValue = String.valueOf(mDataSet.get(position).getName());
        final String codeValue = String.valueOf(mDataSet.get(position).getCode());
        holder.name.setText(nameValue);
        holder.code.setText(codeValue);
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public static class UserViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView name, code, icon;

        UserViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.countries_layout);
            name = (TextView) itemView.findViewById(R.id.name);
            code = (TextView) itemView.findViewById(R.id.code);
            icon = (TextView) itemView.findViewById(R.id.icon);
            code.setTextColor(Color.RED);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
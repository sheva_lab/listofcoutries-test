package org.shevchenko.listofcountries.repository.interfaces;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

public interface IGenericDao<T>  extends IDBLogic{

    long insert(T entity);

    void insertFromCSV(Context context);

    void getEntity(int id);

    ArrayList<T> getAllEntities();

    int getCountOfEntities();

    int updateEntity(T entity);

    void deleteEntity(T entity);

}

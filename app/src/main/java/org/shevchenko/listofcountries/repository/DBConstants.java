package org.shevchenko.listofcountries.repository;

public class DBConstants {
    public static  final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "worldManager";
    public static final String TABLE_COUNTRIES = "countries";
    public static final String COUNTRY_KEY_ID = "id";
    public static final String COUNTRY_KEY_NAME = "name";
    public static final String COUNTRY_KEY_CODE = "code";

    public static  final String DATA_CSV_NAME = "countries.csv";

    private DBConstants() {
    }
}

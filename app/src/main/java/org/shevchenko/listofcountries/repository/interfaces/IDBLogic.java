package org.shevchenko.listofcountries.repository.interfaces;

public interface IDBLogic {

    void close();

    boolean isTableEmpty(String tableName);
}

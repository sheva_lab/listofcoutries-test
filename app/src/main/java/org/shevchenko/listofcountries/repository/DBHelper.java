package org.shevchenko.listofcountries.repository;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import static org.shevchenko.listofcountries.repository.DBConstants.*;

public class DBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "world.db";
    private static final String FOLDER_NAME = "ListOfCountries";
    private static final String DATABASE_PATH = Environment
            .getExternalStorageDirectory().getAbsolutePath()
            + "/" + FOLDER_NAME +"/"
            +DATABASE_NAME;
    private static final int DATABASE_VERSION = 1;

    public DBHelper(Context context) {
        super(context, DATABASE_PATH, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE " + TABLE_COUNTRIES + "(" +
                COUNTRY_KEY_ID + " INTEGER PRIMARY KEY, " +
                COUNTRY_KEY_NAME + " TEXT, " +
                COUNTRY_KEY_CODE + " INTEGER" + ")";
        db.execSQL(query);
        Log.i("SQL: ", query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS" + TABLE_COUNTRIES);
        onCreate(db);
    }
}

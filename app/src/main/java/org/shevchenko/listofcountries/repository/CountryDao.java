package org.shevchenko.listofcountries.repository;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

import org.shevchenko.listofcountries.model.Country;
import org.shevchenko.listofcountries.repository.interfaces.IGenericDao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static org.shevchenko.listofcountries.repository.DBConstants.*;

public class CountryDao implements IGenericDao<Country> {

    private static SQLiteDatabase mDataBase;
    private static Cursor mCursor;

    public CountryDao(Context context) {
        DBHelper mOpenHelper = new DBHelper(context);
        mDataBase = mOpenHelper.getWritableDatabase();
    }

    @Override
    public long insert(Country entity) {
        ContentValues cv = new ContentValues();
        cv.put(COUNTRY_KEY_ID, entity.getId());
        cv.put(COUNTRY_KEY_NAME, entity.getName());
        cv.put(COUNTRY_KEY_CODE, entity.getCode());
        return mDataBase.insert(TABLE_COUNTRIES, null, cv);
    }

    @Override
    public void insertFromCSV(Context context) {
        BufferedReader buffer = getBufferedReader(context);
        mDataBase.beginTransaction();
        readData(buffer);
        mDataBase.setTransactionSuccessful();
        mDataBase.endTransaction();
        mDataBase.close();
    }

    private BufferedReader getBufferedReader(Context context) {
        AssetManager manager = context.getAssets();
        InputStream inStream = null;
        try {
            inStream = manager.open(DATA_CSV_NAME);
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedReader buffer = new BufferedReader(new InputStreamReader(inStream));
        return buffer;
    }

    private void readData(BufferedReader buffer) {
        String line;
        try {
            while ((line = buffer.readLine()) != null) {
                String[] colums = line.split(";");
                if (colums.length != 3) {
                    continue;
                }

                ContentValues cv = new ContentValues(3);
                cv.put(COUNTRY_KEY_ID, colums[0].trim());
                cv.put(COUNTRY_KEY_NAME, colums[1].trim());
                cv.put(COUNTRY_KEY_CODE, colums[2].trim());
                mDataBase.insert(TABLE_COUNTRIES, null, cv);
            }
            buffer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getEntity(int id) {

    }

    @Override
    public ArrayList<Country> getAllEntities() {
        mCursor = mDataBase.query(TABLE_COUNTRIES, null, null, null, null,
                null, COUNTRY_KEY_NAME);
        ArrayList<Country> arr = new ArrayList<>();
        mCursor.moveToFirst();
        if (!mCursor.isAfterLast()) {
            do {
                int number = mCursor.getInt(0);
                String name = mCursor.getString(1);
                int code = mCursor.getInt(2);
                arr.add(new Country(number, name, code));
            } while (mCursor.moveToNext());
        }
        return arr;
    }

    @Override
    public int getCountOfEntities() {
        return 0;
    }

    @Override
    public int updateEntity(Country entity) {
        return 0;
    }

    @Override
    public void deleteEntity(Country entity) {
    }

    @Override
    public void close() {
        if (mCursor != null && !mCursor.isClosed()) {
            mCursor.close();
        }
        if (mDataBase != null) {
            mDataBase.close();
        }
    }

    @Override
    public boolean isTableEmpty(String tableName) {
        return DatabaseUtils.queryNumEntries(mDataBase, tableName) == 0;
    }
}

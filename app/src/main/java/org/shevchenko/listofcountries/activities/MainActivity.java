package org.shevchenko.listofcountries.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.shevchenko.listofcountries.R;
import org.shevchenko.listofcountries.repository.CountryDao;

import static org.shevchenko.listofcountries.repository.DBConstants.*;

public class MainActivity extends AppCompatActivity {

    private AppCompatButton importDataBtn, readDataBtn;
    private CountryDao countryDao;

    private final String TAG = this.getClass().getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViewElements();
        countryDao = new CountryDao(this);
    }

    private  void insertData() {
        if(!countryDao.isTableEmpty(TABLE_COUNTRIES)){
            Toast toast = Toast.makeText(getApplicationContext(), "Data  not imported, table not empty !", Toast.LENGTH_SHORT);
            toast.show();
        }else {
            Log.i(TAG, "Inserting data...");
            countryDao.insertFromCSV(this);
            Toast toast = Toast.makeText(getApplicationContext(), "Data imported...", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private void initViewElements() {
        setContentView(R.layout.main);
        importDataBtn = (AppCompatButton) findViewById(R.id.btn_import_csv);
        readDataBtn = (AppCompatButton) findViewById(R.id.btn_view_data);
        buttonsHandler();
    }

    private void buttonsHandler() {
        importDataBtn.setOnClickListener((View v) ->{
            insertData();
        });

        readDataBtn.setOnClickListener((View v) -> {
            startActivity(new Intent(this, CountriesActivity.class));
        });
    }

}

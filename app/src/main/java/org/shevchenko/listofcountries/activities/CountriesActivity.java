package org.shevchenko.listofcountries.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import org.shevchenko.listofcountries.R;
import org.shevchenko.listofcountries.adapters.CountriesAdapter;
import org.shevchenko.listofcountries.repository.CountryDao;

public class CountriesActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private CountriesAdapter countriesAdapter;
    private CountryDao countryDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViewElements();
        countryDao = new CountryDao(this);
        countriesAdapter = new CountriesAdapter(this,countryDao.getAllEntities());
        recyclerView.setAdapter(countriesAdapter);
        countriesAdapter.notifyDataSetChanged();
    }

    private void initViewElements() {
        setContentView(R.layout.countries);
        recyclerView = (RecyclerView) findViewById(R.id.usersList);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
    }
}
